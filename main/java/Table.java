import java.util.*;
public class Table {
    private char data [][] = {{'-','-','-'},
                      {'-','-','-'},
                      {'-','-','-'}}; ;
    private Player currentPlayer;
    private Player o;
    private Player x;
    private Player win;
     Random r = new Random();
    
public Table (Player x,Player o){
    this.o = o;
    this.x = x;
    if(r.nextInt(2)+ 1 == 1){
        currentPlayer = o;
    }else{
        currentPlayer = x;
    }
    win = null;
}

public Player getcurrentPlayer(){
    return currentPlayer ;
}

public char[][] getdata(){
    return data;
}

public Player getWinner(){
    return win;
}

public void setRowCol(int row,int col){
    if(data [row - 1][col - 1] == '-' ){
       data [row - 1][col - 1] = currentPlayer.getName();
    }else{
        data[10][10] = '5';
    }
}

public void setPoint(){
    if(win == o){
        o.win();
        x.lose();
    }else if(win == x){
        x.win();
        o.lose();
    }
}

public void switchTurn(){
   if(currentPlayer == o){
            currentPlayer = x;
        }else{
            currentPlayer = o;
        }
}

public boolean checkWin(){
        for(int i = 0;i < 3;i++){
            if(checkRow(i)){
                return true;
            }else if(checkCol(i)){
                return true;
            }else if(checkX1()){
                return true;
            }else if(checkX2()){
                return true;
            }
        }
        switchTurn();
        return false;
    }
    public boolean checkRow(int row){
        for(int i = 0; i < 3;i++){
            if(data[row][i] != currentPlayer.getName()){
                return false;
            }
        }
        win = currentPlayer;
        setPoint();
        return true;
        
    }
    public boolean checkCol(int col){
        for(int i = 0;i < 3;i++){
            if(data[i][col] != currentPlayer.getName()){
                return false;
            }
        }
        win = currentPlayer;
        setPoint();
        return true;
    }
    
    public boolean checkX1(){
        for(int i = 0;i < 3;i++){
        if(data[i][i] != currentPlayer.getName()){
                return false;
            }
        }
        win = currentPlayer;
        setPoint();
            return true;
    }
    public boolean checkX2(){
        for(int i = 0;i < 3;i++){
            if(data[2-i][0+i] != currentPlayer.getName()){
                return false;
            }
        }
        win = currentPlayer;
        setPoint();
            return true;    
        }       
    public boolean checkDraw(){
        for(int i = 0;i < 3;i++){
            for(int j = 0;j < 3;j++){
                if(data[i][j] == '-'){
                    return false;
                }
            }
        }
        o.draw();
        x.draw();
        win = null;
        return true;
    }

}
