import java.util.*;
public class Game {
    private Table table;
    private int row;
    private int col;
    private Player o;
    private Player x;
    Scanner scan = new Scanner(System.in);
    
    public Game(){
        o = new Player('O');
        x = new Player('X');
    }
    public void startGame(){
        table = new Table(o,x);
    }
    
    public void showWelcome(){
        System.out.println("Welcome to OX Game !");
    }
    
    public void showTable(){
        char data[][] = table.getdata();
        for(int a = 0; a < 3;a++){
               for(int j = 0; j < 3;j++){
                   System.out.print(" "+data[a][j]);
               }
               System.out.println();
           }
    }
    
    public void showTurn(){
        System.out.println(table.getcurrentPlayer().getName()+" turn");
    }
    
    public boolean inputRowcol(){
        System.out.print("Please input Row and Col : ");
       try{
         int row = scan.nextInt();
         int col = scan.nextInt();
         table.setRowCol(row,col);
         return true;
        }catch (Exception x){
            System.out.println("Please input again!");
            return false;
        }
    }
    
    public boolean inputContinue(){
        System.out.print("Continue? (y/n) :");
        String choice = scan.next();
        if(choice.equals("y")){
            return true;
        }
        return false;
    }
    
    public void showBaBye(){
        System.out.println("Ba Bye!");
    }
    
    public void showWin(Player player){
        showTable();
        if(table.getWinner() == null){
            System.out.println("Draw");
        }else{
            System.out.println("Player "+ player.getName() +" Win");
        }
        System.out.println("O Win : "+ o.getWin()+" Lose : "+o.getLose()+" Draw : "+o.getDraw());
        System.out.println("X Win : "+ x.getWin()+" Lose : "+x.getLose()+" Draw : "+x.getDraw());
    }
    public void run(){
        showWelcome();
        while(true){
           startGame();
           runOne();
           if(!inputContinue()){
               break;
           }
        }
        showBaBye();
    }
    
    public void runOne(){
        while(true){
            showTable();
            showTurn();
            if(inputRowcol()){
                if(table.checkWin()){
                    Player player = table.getWinner();
                    showWin(player);
                    return;
                }else if(table.checkDraw()){
                    Player player = table.getWinner();
                    showWin(player);
                    return;
                }
            }
        }
    }
}
