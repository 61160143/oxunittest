/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author admin
 */
public class TableandPlayerUnitTest {
    
    public TableandPlayerUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    @Test
    public void testcheckRow() {
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,1);
         table.setRowCol(1,2);
         table.setRowCol(1,3);
         assertEquals(true,table.checkWin());
    }
    public void testcheckRow2() {
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(2,1);
         table.setRowCol(2,2);
         table.setRowCol(2,3);
         assertEquals(true,table.checkWin());
    }
    public void testcheckRow3() {
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(3,1);
         table.setRowCol(3,2);
         table.setRowCol(3,3);
         assertEquals(true,table.checkWin());
    }
    public void testcheckCol() {
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,1);
         table.setRowCol(2,1);
         table.setRowCol(3,1);
         assertEquals(true,table.checkWin());
    }
     public void testcheckCol2() {
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,2);
         table.setRowCol(2,2);
         table.setRowCol(3,2);
         assertEquals(true,table.checkWin());
    }
      public void testcheckCol3() {
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,3);
         table.setRowCol(2,3);
         table.setRowCol(3,3);
         assertEquals(true,table.checkWin());
    }
    public void testswitchTurn() {
         Player o = new Player('o');
         Player x = new Player('x'); 
         Table table = new Table(o,x);
         
         if(table.getcurrentPlayer().getName()== 'o'){
             table.switchTurn();
             assertEquals('x',table.getcurrentPlayer().getName());
         }else{
             table.switchTurn();
             assertEquals('o',table.getcurrentPlayer().getName());
         } 
    }
     public void testcheckX1() {
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,1);
         table.setRowCol(2,2);
         table.setRowCol(3,3);
         assertEquals(true,table.checkWin());
    }
     public void testcheckX2() {
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,3);
         table.setRowCol(2,2);
         table.setRowCol(3,1);
         assertEquals(true,table.checkWin());
    }
     public void testsetPoint() {
         Player o = new Player('o');
         Player x = new Player('x');
         Table table = new Table(o,x);
         table.setRowCol(1,3);
         table.setRowCol(2,2);
         table.setRowCol(3,1);
         table.checkWin();
         assertEquals(1,table.getWinner().getWin());
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
}
